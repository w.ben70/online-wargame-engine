/*
  This file contains all top-level boardgame IO definitions for this game
*/
import { INVALID_MOVE } from 'boardgame.io/core';

export const game = {
    setup: () => ({ cells: Array(9).fill(null) }),

    turn: {
      moveLimit: 1,
    },

    // The minimum and maximum number of players supported
    // (This is only enforced when using the Lobby server component.)
    minPlayers: 1,
    maxPlayers: 16,

    phases: {
      deployment: {
        moves: { Deploy },
        start: true,
        endIf: G => G.deployed == true
      },

      starting: {
        moves: { TriggerAbility },
      },
  
      movement: {
        moves: { MoveUnit },
      },

      psychic: {
        moves: { TriggerPsychicAbility },
      },

      shooting: {
        moves: { Shoot },
      },

      melee: {
        moves: { Fight },
      },

      morale: {
        moves: { TestMorale },
      },
    },

    moves: {
      clickCell: (G, ctx, id) => {
        if (G.cells[id] !== null) {
          return INVALID_MOVE;
        }
        G.cells[id] = ctx.currentPlayer;
      },
    },

    // Ends the game if this returns anything.
    // The return value is available in `ctx.gameover`.
    endIf: (G, ctx) => obj,

    // Called at the end of the game.
    // `ctx.gameover` is available at this point.
    onEnd: (G, ctx) => G,
  };