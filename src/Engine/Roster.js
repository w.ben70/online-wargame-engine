/*
  This file is used to serialize/deserialize a player's roster of units
*/

class Terrain {
    constructor(height, width, x, y) {
      this.height = height;
      this.width = width;
      this.x = x;
      this.y = y;
    }
  }